package ch.felline.model_view_presenter_example.login;

/**
 * Created by gfelline on 4/27/17.
 */

public interface LoginRepository {

    User getUser();

    void saveUser(User user);

}
