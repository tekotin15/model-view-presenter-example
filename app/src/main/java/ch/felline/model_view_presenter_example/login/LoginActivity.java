package ch.felline.model_view_presenter_example.login;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import javax.inject.Inject;

import ch.felline.model_view_presenter_example.R;
import ch.felline.model_view_presenter_example.root.App;

public class LoginActivity extends AppCompatActivity implements LoginActivityMVP.View {

    EditText txtFirstName;
    EditText txtLastName;
    Button login;

    @Inject
    LoginActivityMVP.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ((App) getApplication()).getComponent().injectModules(this);

        txtFirstName = (EditText) findViewById(R.id.txtFirstName);
        txtLastName = (EditText) findViewById(R.id.txtLastName);
        login = (Button) findViewById(R.id.btnLogIn);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.setLoginButtonClicked();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.setView(this);
        presenter.getCurrentUser();
    }

    public String getFirstName() {
        return txtFirstName.getText().toString();
    }

    public String getLastName() {
        return txtLastName.getText().toString();
    }

    @Override
    public void showUserNotAvailable() {
        Toast.makeText(this, "Error the user is not available", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showInputError() {
        Toast.makeText(this, "First Name or Last Name cannot be emtpy", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showUserSavedMessage() {
        Toast.makeText(this, "User saved ", Toast.LENGTH_SHORT).show();
    }

    public void setFirstName(String firstName) {
        txtFirstName.setText(firstName);
    }

    public void setLastName(String lastName) {
        txtLastName.setText(lastName);
    }
}
