package ch.felline.model_view_presenter_example.login;

/**
 * Created by gfelline on 4/27/17.
 */

public interface LoginActivityMVP {

    interface View {

        String getFirstName();
        String getLastName();

        void showUserNotAvailable();
        void showInputError();
        void showUserSavedMessage();

        void setFirstName(String firstName);
        void setLastName(String lastName);

    }

    interface Presenter {
        void setView(LoginActivityMVP.View view);

        void setLoginButtonClicked();

        void getCurrentUser();
    }

    interface Model {

        void createUser(String firstName, String lastName);

        User getUser();

    }

}
