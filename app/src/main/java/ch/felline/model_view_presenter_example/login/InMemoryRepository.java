package ch.felline.model_view_presenter_example.login;

/**
 * Created by gfelline on 4/27/17.
 */

public class InMemoryRepository implements LoginRepository{

    private User user;

    @Override
    public User getUser() {

        if (user == null) {
            User user = new User("Fox", "Mulder");
            user.setId(0);
            return user;
        } else {
            return user;
        }


    }

    @Override
    public void saveUser(User user) {
        if (user == null) {
            user = getUser();
        }

        this.user = user;
    }
}
