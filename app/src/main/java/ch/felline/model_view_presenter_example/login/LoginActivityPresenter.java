package ch.felline.model_view_presenter_example.login;

import android.support.annotation.Nullable;

/**
 * Created by gfelline on 4/27/17.
 */

public class LoginActivityPresenter implements LoginActivityMVP.Presenter {


    @Nullable
    private LoginActivityMVP.View view;

    private LoginActivityMVP.Model model;

    public LoginActivityPresenter(LoginActivityMVP.Model model) {
        this.model = model;
    }

    @Override
    public void setView(LoginActivityMVP.View view) {
        this.view = view;
    }

    @Override
    public void setLoginButtonClicked() {
        if (view != null) {
            if ("".equals(view.getFirstName().trim()) || "".equals(view.getLastName().trim())) {
                view.showInputError();
            } else {
                model.createUser(view.getFirstName(), view.getLastName());
                view.showUserSavedMessage();
            }
        }
    }

    @Override
    public void getCurrentUser() {
        User user = model.getUser();
        if (user == null) {
            if (view != null) {
                view.showUserNotAvailable();
            } else {
                view.setFirstName(user.getFirstName());
                view.setLastName(user.getLastName());

            }
        }
    }
}
