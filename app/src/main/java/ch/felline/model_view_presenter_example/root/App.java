package ch.felline.model_view_presenter_example.root;

import android.app.Application;

import ch.felline.model_view_presenter_example.login.InMemoryRepository;
import ch.felline.model_view_presenter_example.login.LoginModule;
import ch.felline.model_view_presenter_example.login.LoginRepository;

/**
 * Created by gfelline on 4/27/17.
 */

public class App extends Application {

    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .loginModule(new LoginModule())
                .build();
    }

    public ApplicationComponent getComponent() {
        return component;
    }
}
