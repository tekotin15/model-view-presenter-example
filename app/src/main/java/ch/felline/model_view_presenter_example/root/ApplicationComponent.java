package ch.felline.model_view_presenter_example.root;

import javax.inject.Singleton;

import ch.felline.model_view_presenter_example.login.LoginActivity;
import ch.felline.model_view_presenter_example.login.LoginModule;
import dagger.Component;

/**
 * Created by gfelline on 4/27/17.
 */

@Singleton
@Component(modules = {ApplicationModule.class, LoginModule.class})
public interface ApplicationComponent {

    public void injectModules(LoginActivity activity);
}
